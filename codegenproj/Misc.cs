﻿using System;
namespace codegenproj
{
    public class Misc
    {
		public static string Lo(string s) {
			return char.ToLower(s[0]) + s.Substring(1);
		}

		public static string Up(string s)
        {
            return char.ToUpper(s[0]) + s.Substring(1);
        }
        
    }
}
