﻿using System;
namespace codegenproj
{
    public class EnumerationEntry
    {
        public string Name { get; }
        public long Value { get; }
        public bool HasValue { get; }
        public EnumerationEntry(string name, long value, bool hasValue)
        {
            Name = name;
            Value = value;
            HasValue = hasValue;
        }

        public static EnumerationEntry FromString(string s) {
            if(s.Contains("=")) {
                var split = s.Split('=');
                return new EnumerationEntry(split[0], long.Parse(split[1]), true);
            } else {
                return new EnumerationEntry(s,0,false);
            }
        }
    }
}
