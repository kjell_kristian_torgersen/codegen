﻿using System;
using System.Linq;

namespace codegenproj
{
    public class Enumeration
    {
        public string Name { get; }
        public EnumerationEntry[] Entries { get; }

        public Enumeration(string name, EnumerationEntry[] entries)
        {
            Name = name;
            Entries = entries;
        }

        public static Enumeration FromString(string s) {
            var split = s.Split(';');
            var name = split[0];
            var entries = split.Skip(1).Select(EnumerationEntry.FromString).ToArray();
            return new Enumeration(name, entries);
        }
    }
}
