﻿using System.Collections.Generic;
using System.Linq;

namespace codegenproj
{
    class CppGen
	{
		private static string[] cppTypes = new string[] {"void", "bool", "uint8_t", "uint16_t", "uint32_t", "uint64_t", "int8_t", "int16_t", "int32_t", "int64_t", "std::string", "float", "double" };

   		private static string ToVariableHpp(Variable v) {

			var type = "";
			if (v.Interface)
			{
				type = "" + v.Class + "* "; // unique
			}
			else { 
				type = v.Type == VariableType.Class ? v.Class : cppTypes[(int)v.Type];
			}

			if (v.Array)
			{
				return "std::vector<" + type + "> " + Misc.Lo(v.Name);
			} else { 
				return type + " " + Misc.Lo(v.Name);
			}
		}

		private static string ToConstructorVariableHpp(Variable v)
        {
			if (v.Array)
			{
				
				return "" + Type(v) + " " + Misc.Lo(v.Name);
			}
			else {
				if (v.Type == VariableType.Class)
				{
					if (v.Interface)
					{
						return Type(v) + " " + Misc.Lo(v.Name);
					}else{
						return "const " + Type(v) + "& " + Misc.Lo(v.Name);
					}
				}
				else { 
					return "" + Type(v) + " " + Misc.Lo(v.Name);
				}
			}

        }

		private static string ToGetterHpp(Variable v)
		{
			if (v.Array)
			{
				//var type = v.Type == VariableType.Class ? v.Class : cppTypes[(int)v.Type];
				return "const " + Type(v) + "& " + Misc.Up(v.Name) + "() const";
			}
			else
			{
                if (v.Interface)
                {
                    return "" + Type(v) + " " + Misc.Up(v.Name) + "() const";
                } else {
                    if (v.Type == VariableType.Class)
                    {
                        return "const " + Type(v) + "& " + Misc.Up(v.Name) + "() const";
                    } else {
                        return "" + Type(v) + " " + Misc.Up(v.Name) + "() const";
                    }
                }
            }
		}

		private static string ToGetterCpp(Variable v, string Class)
        {
            if (v.Array)
			{
				//var type = v.Type == VariableType.Class ? v.Class : cppTypes[(int)v.Type];
				return "const " + Type(v) + "& " + Class + "::" + Misc.Up(v.Name) + "() const";
			}
			else
			{
                if (v.Interface || (v.Type != VariableType.Class))
                {
                    return "" + Type(v) + " " + Class + "::" + Misc.Up(v.Name) + "() const";
                } else {
                    return "const " + Type(v) + "& " + Class + "::" + Misc.Up(v.Name) + "() const";
                }
            }
        }

		private static string[] GetCustom(Variable[] vars) { 
			List<string> custom = new List<string>();

            foreach (var m in vars)
            {
                if (m.Type == VariableType.Class)
                {
                    if (!custom.Contains(m.Class))
                    {
                        custom.Add(m.Class);
                    }
                }
            }
			return custom.ToArray();
		}

        public static IEnumerable<string> EnumHpp(Enumeration en)
        {

            yield return "#ifndef __" + en.Name.ToUpper() + "__";
            yield return "#define __" + en.Name.ToUpper() + "__";
            yield return "";
            yield return "#include <iostream>";
            yield return "";
            yield return "enum class " + en.Name + " {";
            foreach(var entry in en.Entries) { 
                if(entry.HasValue) {
            
                    yield return "\t" + entry.Name + " = " + entry.Value + ", ";
                } else {
                    yield return "\t" + entry.Name + ", ";
                }
            }
            yield return "};";
            yield return "";
            yield return "std::ostream& operator<<(std::ostream& os, const " + en.Name + " & data);";
            yield return "";
            yield return "#endif // __" + en.Name.ToUpper() + "__";
        }

        public static IEnumerable<string> EnumCpp(Enumeration en)
        {
            yield return "#include <iostream>";
            yield return "";
            yield return "#include \"" + en.Name + ".hpp\"";
            yield return "";
            yield return "std::ostream& operator<<(std::ostream& os, const " + en.Name + " & data) {";
            yield return "\tos << \"" + en.Name + "::\";";
            yield return "\tswitch(data) {";
            foreach (var entry in en.Entries)
            {
                yield return "\t\tcase " + en.Name + "::" + entry.Name + ":";
                yield return "\t\t\tos << \"" + entry.Name + "\";";
                yield return "\t\tbreak;";
            }
            yield return "\t}";
            yield return "\treturn os;";
            yield return "}";
        }

        private static string[] GetICustom(Interface i) { 
			List<string> custom = new List<string>();

			foreach (var met in i.Methods)
            {
				foreach (var m in met.Arguments)
				{
					if (m.Type == VariableType.Class)
					{
						if (!custom.Contains(m.Class))
						{
							custom.Add(m.Class);
						}
					}
				}
				if (met.Name.Type == VariableType.Class) { 
					if (!custom.Contains(met.Name.Class))
                    {
                        custom.Add(met.Name.Class);
                    }
				}
            }
            custom.Sort();
            return custom.ToArray();
		}

        private static string[] GetCustom2(Method[] methods, Variable[] variables, string[] ifaces)
        {
            List<string> custom = new List<string>();
            foreach(var i in ifaces) {
                if (!custom.Contains(i))
                {
                    custom.Add(i);
                }
            }
            foreach (var v in variables) {
                if (v.Type == VariableType.Class)
                {
                    if (!custom.Contains(v.Class))
                    {
                        custom.Add(v.Class);
                    }
                }
            }

            foreach (var met in methods)
            {
                foreach (var m in met.Arguments)
                {
                    if (m.Type == VariableType.Class)
                    {
                        if (!custom.Contains(m.Class))
                        {
                            custom.Add(m.Class);
                        }
                    }
                }
                if (met.Name.Type == VariableType.Class)
                {
                    if (!custom.Contains(met.Name.Class))
                    {
                        custom.Add(met.Name.Class);
                    }
                }
            }
            custom.Sort();
            return custom.ToArray();
        }

        public static IEnumerable<string> InterfaceHpp(Interface i)
        {
            yield return "#ifndef __" + i.Name.ToUpper() + "__";
            yield return "#define __" + i.Name.ToUpper() + "__";
            yield return "";
            yield return "#include <cstdint>";
            yield return "#include <vector>";
            yield return "#include <string>";
            yield return "";

            var customIncludes = GetICustom(i);
            if (customIncludes.Count() != 0)
            {
                foreach (var cu in customIncludes)
                {
                    yield return "#include \"" + cu + ".hpp\"";
                }

                yield return "";
            }
            yield return "// Abstract Base Class Interface";
            yield return "class " + i.Name + " {";
            yield return "public:";

			foreach (var m in i.Methods)
            {
                yield return "\tvirtual " + ToBaseMethodHpp(m) + " = 0;";
            }
			yield return "\tvirtual ~" + i.Name + "() { }";
            yield return "};";
            yield return "";
            yield return "#endif // __" + i.Name.ToUpper() + "__";
        }

		private static string ToBaseMethodVarHpp(Variable v)
        {
			if (v.Type == VariableType.Class && !v.Array)
			{
				return Type(v) + " " + Misc.Up(v.Name);
			}
			else { 
				return Type(v) + " " + Misc.Up(v.Name);
			}
        }

		private static string ToBaseMethodVarHppLo(Variable v)
        {
            if (v.Type == VariableType.Class && !v.Interface || v.Array)
			{
				return "const "+Type(v) + "& " + Misc.Lo(v.Name);
			}
			else { 
				return Type(v) + " " + Misc.Lo(v.Name);
			}
        }

		private static string ToBaseMethodHpp(Method m)
		{
			var arglist = string.Join(", ", m.Arguments.Select(ToBaseMethodVarHppLo));
			return ToBaseMethodVarHpp(m.Name) + "(" + arglist + ")";
		}

		private static string Type(Variable v)
		{
			string type;

			if (v.Array)
			{
				
				if (v.Interface)
				{
					type = "" + v.Class + "* ";
				}
				else { 
					type = v.Type == VariableType.Class ? v.Class : cppTypes[(int)v.Type];
				}
				return "std::vector<" + type + "> ";
			}
			else
			{
				if (v.Interface)
				{
					type = "" + v.Class + "* ";
				}
				else
				{
					type = v.Type == VariableType.Class ? v.Class : cppTypes[(int)v.Type];
				}
				return type;
			}
		}
        		
        public static IEnumerable<string> ClassHpp(string name, Class c)
        {
            yield return "#ifndef __" + name.ToUpper() + "__";
            yield return "#define __" + name.ToUpper() + "__";
            yield return "";
            yield return "#include <cstdint>";
            yield return "#include <vector>";
            yield return "#include <string>";
			if (c.Interfaces.Count() != 0) { 
				yield return "#include <memory>";
			}
            yield return "";

            foreach (var cu in GetCustom2(c.Methods.ToArray(), c.Variables.ToArray(), c.Interfaces.ToArray()))
            {
                yield return "#include \"" + cu + ".hpp\"";
            }

            yield return "";
            if (c.Interfaces.Count() == 0)
            {
                yield return "// Immutable data class";
                yield return "class " + name + " {";
            }
            else
            {
                yield return "// Interface implementation class";
                var ifacelist = string.Join(", ", c.Interfaces.Select(arg => "public " + arg));
                yield return "class " + name + ": " + ifacelist + " {";
            }
            yield return "public:";
            if (c.Variables.Count() > 0)
            {
                yield return "\t// Constructor ";
                var arglist = string.Join(", ", c.Variables.Select(ToConstructorVariableHpp));
                yield return "\t" + name + "(" + arglist + ");";
                yield return "";
                yield return "\t// Getters ";
                foreach (var m in c.Variables)
                {
                    yield return "\t" + ToGetterHpp(m) + ";";
                }
                yield return "";
            }

            if (c.Methods.Count() > 0)
            {
                yield return "\t// Methods specified by interface(s) ";
                foreach (var m in c.Methods)
                {
                    if (m.Virtual)
                    {
                        yield return "\tvirtual " + ToBaseMethodHpp(m) + ";";
                    }
                }
                yield return "";
                yield return "\t// Other methods";
                foreach (var m in c.Methods)
                {
                    if (!m.Virtual)
                    {
                        yield return "\t" + ToBaseMethodHpp(m) + ";";
                    }
                }
                yield return "";
            }
            //yield return "\tfriend std::ostream& operator<<(std::ostream& os, const " + name + "& " + name.ToLower() + ");";
            yield return "\t// Destructor ";
            yield return "\tvirtual ~" + name + "();";
            yield return "";
            yield return "private:";
            foreach (var m in c.Variables)
            {
                yield return "\t" + ToVariableHpp(m) + ";";
            }

            yield return "};";
            yield return "";
            yield return "std::ostream& operator<<(std::ostream& os, const " + name + "& " + name.ToLower() + ");";
            yield return "#endif // __" + name.ToUpper() + "__";
        }

		private static string ConstructorInit(Variable variable) {
			if (variable.Array || variable.Interface)
			{
				return Misc.Lo(variable.Name) + "(std::move(" + Misc.Lo(variable.Name) + "))";
			}
			else {
				return Misc.Lo(variable.Name) + "(" + Misc.Lo(variable.Name) + ")";
			}
		}

        public static IEnumerable<string> ClassCpp(string name, Class c)
        {
            yield return "#include <cstdint>";
            yield return "#include <vector>";
            yield return "#include <string>";
            yield return "#include <iostream>";
			if (c.Interfaces.Count() != 0)
            {
                yield return "#include <memory>";
            }
            yield return "";
            yield return "#include \"" + name + ".hpp\"";

            foreach (var cu in GetCustom2(c.Methods.ToArray(), c.Variables.ToArray(), c.Interfaces.ToArray()))
            {
                yield return "#include \"" + cu + ".hpp\"";
            }

            yield return "";


            if (c.Variables.Count() > 0)
            {
                yield return "// Constructor ";
                var arglist = string.Join(", ", c.Variables.Select(ToConstructorVariableHpp));
				var inits = string.Join(", ", c.Variables.Select(ConstructorInit));
                yield return name + "::" + name + "(" + arglist + ") :";
                yield return "\t"+inits + " { }";
                yield return "";

                yield return "// Getters ";
                foreach (var m in c.Variables)
                {

                    yield return ToGetterCpp(m, name) + "{";
                    yield return "\treturn this->" + Misc.Lo(m.Name) + ";";
                    yield return "}";
                    yield return "";
                }
            
            }
            if (c.Methods.Count() > 0)
            {
                yield return "// Methods specified by interface(s)";
                foreach (var method in c.Methods)
                {
                    var arglist2 = string.Join(", ", method.Arguments.Select(ToBaseMethodVarHppLo));
					var signature = "";
					if (method.Name.Type == VariableType.Class && !method.Name.Array)
					{
						signature = Type(method.Name) + " " + name + "::" + method.Name.Name + "(" + arglist2 + ")";
					}
					else { 
						signature = Type(method.Name) + " " + name + "::" + method.Name.Name + "(" + arglist2 + ")";
					}
                    yield return signature + " {";
                    yield return "#warning " + signature + " not implemented";
                    yield return "\tstd::cout << __FILE__ << \":\" << __LINE__ << \": " + signature + " was called but is not implemented.\" << std::endl;";
                    yield return "}";
                    yield return "";
                }
            }
            yield return "// Destructor ";
            yield return name + "::~" + name + "() {"; 
            foreach(var v in c.Variables) { 
            if(v.Array && v.Interface) {
                    yield return "\tfor(auto& x : this->" + Misc.Lo(v.Name) + ") {";
                    yield return "\t\tdelete x;";
                    yield return "\t}";
                }
            }
            yield return "}";
            yield return "";
            yield return "std::ostream & operator<< (std::ostream & os, const " + name + "& " + name.ToLower() + ") {";
            //string mem = string.Join(" << ", c.Members.Select(m => '"' + m.Name.ToLower() + "=\" << this->" + m.Name.ToLower()));
            //yield return "\tos << " + mem + ";";
            yield return "\tos << \"" + name + " {\";";
            foreach (var m in c.Variables)
            {
                if (!m.Array)
                {
                    if (m.Type == VariableType.Class || m.Type == VariableType.String || m.Type == VariableType.Float || m.Type == VariableType.Double)
                    {
                        yield return "\tos << \" " + m.Name.ToLower() + "=\" << " + name.ToLower() + "." + Misc.Up(m.Name) + "() << \",\";";
                    }
                    else
                    {
                        yield return "\tos << \" " + m.Name.ToLower() + "=\" << ((int64_t)" + name.ToLower() + "." + Misc.Up(m.Name) + "()) << \",\";";
                    }
                }
                else
                {
                    //yield return "\tos << \" " + m.Name.ToLower() + "=\" << " + c.Name.ToLower() + "." + m.Name.ToLower() + " << \",\";";
                }
            }
            yield return "\tos << \"}\";";
            yield return "\treturn os;";
            yield return "}";
        }
    }
}
