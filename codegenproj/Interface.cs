﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace codegenproj
{
    public class Interface
    {
		public string Name { get; }
		public Method[] Methods { get; }
		public Variable[] Variables { get; }
		public string[] Implementations { get; }

		public Interface(string name, Method[] methods, Variable[] variables, string[] implementations)
		{
			Name = name;
			Methods = methods;
			Variables = variables;
			Implementations = implementations;
		}

		public static Interface FromString(string s) {
			var split = s.Split(';');
			var name = split[0];
			//var methods = split.Skip(1).Take(split.Length - 2).Select(x=>Method.FromString(x,true)).ToArray();
			var implementations = split.Last().Split(',').ToArray();

			var methods = new List<Method>();
            var variables = new List<Variable>();
            
			var smembers = split.Skip(1).Take(split.Length-2);

            foreach (var member in smembers)
            {
                if (member.Contains("("))
                {
                    methods.Add(Method.FromString(member, true));
                }
                else
                {
                    variables.Add(Variable.FromString(member));
                }
            }

			return new Interface(name, methods.ToArray(), variables.ToArray(), implementations);
		}
	}
}
