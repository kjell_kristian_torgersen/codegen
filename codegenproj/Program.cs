﻿using System;
using System.Collections.Generic;
using System.IO;

namespace codegenproj
{
    class MainClass
    {
        static int files = 0;
        public static void WriteFile(string filename, IEnumerable<string> lines)
        {
            File.WriteAllLines(filename, lines);
            Console.WriteLine("Writing " + filename);
            files++;
        }

        static Dictionary<string, Class> classes = new Dictionary<string, Class>();

        public static void AddVariables(string classname, Variable[] variables)
        {
            if (!classes.ContainsKey(classname))
            {
                classes.Add(classname, new Class());
            }
            classes[classname].Variables.AddRange(variables);
        }

        public static void AddMethods(string classname, Method[] methods)
        {
            if (!classes.ContainsKey(classname))
            {
                classes.Add(classname, new Class());
            }
            classes[classname].Methods.AddRange(methods);
        }

        public static void AddInterface(string classname, string iface)
        {
            if (!classes.ContainsKey(classname))
            {
                classes.Add(classname, new Class());
            }
            classes[classname].Interfaces.Add(iface);
        }

        public static void Main(string[] args)
        {
            Directory.CreateDirectory("cpp");
            var headers = Path.Combine("cpp", "headers");
            var enumspath = Path.Combine("cpp", "enums");
            var ifaces = Path.Combine("cpp", "interfaces");
            var implementations = Path.Combine("cpp", "implementations");
            var enums = new List<Enumeration>();
            Directory.CreateDirectory(Path.Combine("cs", "interfaces"));
            Directory.CreateDirectory(Path.Combine("cs", "enums"));
            Directory.CreateDirectory(enumspath);
            Directory.CreateDirectory(headers);
            Directory.CreateDirectory(ifaces);
            List<Interface> interfaces = new List<Interface>();

            if (args.Length > 0)
            {
                int linenr = 0;
                foreach (var line in File.ReadLines(args[0]))
                {
                    ParseLine(enums, interfaces, line, linenr);
                    linenr++;
                }
            } else {
                string line = "";
                int linenr = 0;
                do
                {
                    Console.Write("> ");
                    line = Console.ReadLine();
                    ParseLine(enums, interfaces, line, linenr);
                    linenr++;
                } while (!string.IsNullOrEmpty(line));
            }


            foreach (var i in interfaces)
            {
                //var path = Path.Combine("cpp", i.Name);
                //Directory.CreateDirectory(path);

                WriteFile(Path.Combine(ifaces, i.Name + ".hpp"), CppGen.InterfaceHpp(i));
                WriteFile(Path.Combine("cs","interfaces", i.Name + ".cs"), CsGen.ToCsIf(i));
            }

            foreach (var kv in classes)
            {
                var name = kv.Key;
                var impl = kv.Value;
                WriteFile(Path.Combine(headers, name + ".hpp"), CppGen.ClassHpp(name, impl));
                if (impl.Interfaces.Count == 0)
                {
                    Directory.CreateDirectory(Path.Combine("cpp", "data"));
                    WriteFile(Path.Combine("cpp", "data", name + ".cpp"), CppGen.ClassCpp(name, impl));
                    Directory.CreateDirectory(Path.Combine("cs", "data"));
                    WriteFile(Path.Combine("cs", "data", name + ".cs"), CsGen.ToCs(name, impl));
                }
                else if (impl.Interfaces.Count == 1)
                {
                    Directory.CreateDirectory(Path.Combine("cpp", impl.Interfaces[0]));
                    WriteFile(Path.Combine("cpp", impl.Interfaces[0], name + ".cpp"), CppGen.ClassCpp(name, impl));
                    Directory.CreateDirectory(Path.Combine("cs", impl.Interfaces[0]));
                    WriteFile(Path.Combine("cs", impl.Interfaces[0], name + ".cs"), CsGen.ToCs(name, impl));
                }
                else
                {
                    Directory.CreateDirectory(Path.Combine("cpp", "implementations"));
                    WriteFile(Path.Combine("cpp", "implementations", name + ".cpp"), CppGen.ClassCpp(name, impl));
                    Directory.CreateDirectory(Path.Combine("cs", "implementations"));
                    WriteFile(Path.Combine("cs", "implementations", name + ".cs"), CsGen.ToCs(name, impl));
                }
            }

            foreach (var en in enums)
            {
                WriteFile(Path.Combine(headers, en.Name + ".hpp"), CppGen.EnumHpp(en));
                WriteFile(Path.Combine(enumspath, en.Name + ".cpp"), CppGen.EnumCpp(en));
                WriteFile(Path.Combine("cs", "enums", en.Name + ".cs"), CsGen.EnumCs(en));

            }

                Console.WriteLine();
            Console.WriteLine("Wrote " + files + " files.");
            /*string[] test = new string[] { "U8 test1", "U8 test2{}", "U8[] test3", "U8[] test4{}","T test5", "T test6{}", "T[] test7", "T[] test8{}" };
			foreach (var t in test) {
				var v = Variable.FromString(t);
				Console.Write(t + ": ");
				Console.WriteLine(v);
			}*/
            //Method.FromString("U8[] Method1(U8 arg1, T[] arg2)");
            //var d = DataClass.FromString("Student;String Navn;U8 Alder;String[] Fag");
            //WriteFile(d.Name + ".cpp", CppGen.DataCpp(d));
            //WriteFile(d.Name + ".hpp", CppGen.DataHpp(d));
            //var lines = CppGen.DataCpp(d);
            /*foreach (var line in lines) {
				Console.WriteLine(line);
			}*/
            /*var i = Interface.FromString("IControl;Void Draw();Void Click(U16 x,U16 y);Button,TextBox,ComboBox");

			foreach (var line in CppGen.InterfaceHpp(i))
            {
                Console.WriteLine(line);
            }

			foreach (var line in CppGen.ImplementationHpp(i, "Button"))
            {
                Console.WriteLine(line);
            }

			foreach (var line in CppGen.ImplementationCpp(i, "Button"))
			{
				Console.WriteLine(line);
			}*/
        }

        private static void ParseLine(List<Enumeration> enums, List<Interface> interfaces, string line, int linenr)
        {
            if (!string.IsNullOrEmpty(line))
            {
                try
                {
                    switch (line[0])
                    {
                        case '%':
                            ParseInterface(interfaces, line);
                            break;
                        case '#':
                            enums.Add(Enumeration.FromString(line.Substring(1)));
                            break;
                        default:
                            var d = DataClass.FromString(line);
                            AddVariables(d.Name, d.Variables);
                            AddMethods(d.Name, d.Methods);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error parsing line " + linenr + ": " + ex.Message);
                    Console.WriteLine(line);
                }
            }
        }

        private static void ParseInterface(List<Interface> interfaces, string line)
        {
            var i = Interface.FromString(line.Substring(1));
            interfaces.Add(i);

            foreach (var impl in i.Implementations)
            {
                AddMethods(impl, i.Methods);
                AddVariables(impl, i.Variables);
                if (!classes[impl].Interfaces.Contains(i.Name))
                {
                    classes[impl].Interfaces.Add(i.Name);
                }
            }
        }
    }
}
