﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace codegenproj
{
    public class DataClass
    {
		public Variable[] Variables { get; }
        public Method[] Methods { get; }
        public string Name { get; }

		public DataClass(string name, Variable[] variables, Method[] methods)
		{
			Variables = variables;
			Name = name;
            Methods = methods;
		}

		public static DataClass FromString(string s0) {
            var methods = new List<Method>();
            var variables = new List<Variable>();
			
            var split = s0.Split(';');
			var name = split[0];
			var smembers = split.Skip(1).ToArray();
            foreach(var member in smembers) {
                if (member.Contains("(")) {
                    methods.Add(Method.FromString(member, false));    
                } else {
                    variables.Add(Variable.FromString(member));
                }
            }
            return new DataClass(name, variables.ToArray(), methods.ToArray());
		}
	}
}
