﻿using System;
using System.Linq;

namespace codegenproj
{
    public class Method
    {
		public Variable Name { get; }
		public Variable[] Arguments { get; }
        public bool Virtual { get; }
        public Method(Variable name, Variable[] arguments, bool @virtual)
		{
			Name = name;
			Arguments = arguments;
            Virtual = @virtual;
		}

		public static Method FromString(string s, bool @virtual)
		{
			var first = new string(s.TakeWhile((arg) => arg != '(').ToArray());
			var name = Variable.FromString(first);
			var start = s.IndexOf('(') + 1;
			var end = s.IndexOf(')');
			var n = end - start;
			if (n != 0)
			{
				var arglist = s.Substring(start, n);
				var split = arglist.Split(',');
				var arglist2 = split.Select(Variable.FromString).ToArray();

				return new Method(name, arglist2, @virtual);
			} else {
				return new Method(name, new Variable[0] { }, @virtual);
			}
		}

		public override string ToString()
		{
			var args = string.Join(", ", Arguments.Select(a => "{"+a.ToString()+"}"));
            return "Name={" + Name + "}, Arguments={" + args + "}" + ", Virtual=" + Virtual;
		}
	}
}
