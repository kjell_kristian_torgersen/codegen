﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace codegenproj
{
    public class CsGen 
    {
        private static string[] csTypes = new string[] { "void", "bool", "byte", "ushort", "uint", "ulong", "sbyte", "short", "int", "long", "string", "float", "double" };

        public static string Type(Variable v) {
            //var arr = v.Array ? "[]" : "";
            var type = "";
            if(v.Type == VariableType.Class) {
                type = v.Class;
            } else {
                type = csTypes[(int)v.Type];
            }
            if (v.Array)
            {
                return "List<" + type + ">";
            }
            else
            {
                return type;
            }
        }

        public static IEnumerable<string> ToCs(string name, Class c) {
                yield return "using System;";
                yield return "using System.Collections.Generic;";
                          if (c.Interfaces.Count() == 0)
            {
                yield return "// Immutable data class";
                yield return "class " + name + " {";
            }
            else
            {

                yield return "// Interface implementation class";
                var ifacelist = string.Join(", ", c.Interfaces.Select(arg => arg));
                yield return "class " + name + " : " + ifacelist + " {";
            }

            if (c.Variables.Count() > 0)
            {
                yield return "\t// Getters ";
                foreach (var m in c.Variables)
                {
                    yield return "\tpublic " + Type(m) + " " + Misc.Up(m.Name) + " { get; }";
                }
                yield return "";

                yield return "\t// Constructor ";
                var arglist = string.Join(", ", c.Variables.Select(v => Type(v) + " " + Misc.Lo(v.Name)));
                yield return "\tpublic " + name + "(" + arglist + ") {";
                foreach(var v in c.Variables) {
                    yield return "\t\tthis." + Misc.Up(v.Name) + " = " + Misc.Lo(v.Name) + ";";
                }
                yield return "\t}";
                yield return "";
            }

            if (c.Methods.Count() > 0)
            {
                yield return "\t// Methods specified by interface(s) ";
                foreach (var m in c.Methods)
                {
                    if (m.Virtual)
                    {
                        var arglist = string.Join(", ", m.Arguments.Select(v => Type(v) + " " + Misc.Lo(v.Name)));
                        yield return "\tpublic " +Type(m.Name) +" "+ Misc.Up(m.Name.Name) + "("+arglist+") { ";
                        yield return "\t\tthrow new NotImplementedException();";
                        yield return "\t}";
                    }
                }
                yield return "";
                yield return "\t// Other methods";
                foreach (var m in c.Methods)
                {
                    if (!m.Virtual)
                    {
                        var arglist = string.Join(", ", m.Arguments.Select(v => Type(v) + " " + Misc.Lo(v.Name)));
                        yield return "\tpublic " + Type(m.Name) + " " + Misc.Up(m.Name.Name) + "(" + arglist + ") { ";
                        yield return "\t\tthrow new NotImplementedException();";
                        yield return "\t}";
                    }
                }
                yield return "";
            }
            if (c.Variables.Count != 0)
            {
                yield return "\tpublic override string ToString() {";
                yield return "\t\treturn \"" + name + " { \"";
                foreach (var v in c.Variables)
                {
                    yield return "\t\t+ \"" + v.Name + " = \" + this." + Misc.Up(v.Name) + ".ToString() + \", \"";
                }
                yield return "\t\t;";
                yield return "\t}";
            }
            yield return "}";
            //yield return "\tfriend std::ostream& operator<<(std::ostream& os, const " + name + "& " + name.ToLower() + ");";
            /*yield return "\t// Destructor ";
            yield return "\tvirtual ~" + name + "();";
            yield return "";
            yield return "private:";
            foreach (var m in c.Variables)
            {
                yield return "\t" + ToVariableHpp(m) + ";";
            }

            yield return "};";
            yield return "";
            yield return "std::ostream& operator<<(std::ostream& os, const " + name + "& " + name.ToLower() + ");";
            yield return "#endif // __" + name.ToUpper() + "__";*/
        }

        public static IEnumerable<string> ToCsIf(Interface i) {
            yield return "using System;";
            yield return "using System.Collections.Generic;";
            yield return "interface " + i.Name + " {";
            if (i.Variables.Length != 0)
            {
                yield return "\t// Getters ";
                foreach (var m in i.Variables)
                {
                    yield return "\t" + Type(m) + " " + Misc.Up(m.Name) + " { get; }";
                }
                yield return "";
            }
            if (i.Methods.Length != 0)
            {
                yield return "\t// Methods ";
                foreach (var m in i.Methods)
                {
                    var arglist = string.Join(", ", m.Arguments.Select(v => Type(v) + " " + Misc.Lo(v.Name)));
                    yield return "\t" + Type(m.Name) + " " + Misc.Up(m.Name.Name) + "(" + arglist + ");";
                }
            }
            yield return "}";
        }

        public static IEnumerable<string> EnumCs(Enumeration en)
        {

           
            yield return "enum " + en.Name + " {";
            foreach (var entry in en.Entries)
            {
                if (entry.HasValue)
                {

                    yield return "\t" + entry.Name + " = " + entry.Value + ", ";
                }
                else
                {
                    yield return "\t" + entry.Name + ", ";
                }
            }
            yield return "};";
        }
    }
}
