﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace codegenproj
{
    public class Class
    {
        // public string Name { get; }
        public List<Variable> Variables { get; } = new List<Variable>();
        public List<Method> Methods { get; } = new List<Method>();
        public List<string> Interfaces { get; } = new List<string>();

        /*public Class(string name, Variable[] variables, Method[] methods)
        {
            Name = name;
            Variables = variables;
            Methods = methods;
        }*/
        /*
        public static Class FromString(string s) {
            var split = s.Split(';');
            var name = split[0];
            var vars = split[1].Split(',').Select(Variable.FromString).ToArray();
            var methods = split.Skip(2).Select(Method.FromString).ToArray();
            return new Class(name, vars, methods);
        }*/
    }
}
