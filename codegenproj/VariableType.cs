﻿using System;
namespace codegenproj
{
    public enum VariableType
    {
		Void, Bool, U8, U16, U32, U64, S8, S16, S32, S64, String, Float, Double, Class
    }
}
