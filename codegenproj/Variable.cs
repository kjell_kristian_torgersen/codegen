﻿using System;
namespace codegenproj
{
    public class Variable
    {
		public VariableType Type { get; }
		public string Class { get; }
		public string Name { get; }
		public bool Array { get; }
		public bool Setter { get; }
		public bool Interface { get { return Type == VariableType.Class && Class[0]=='I'; } }

		public Variable(VariableType type, string @class, string name, bool array, bool setter)
		{
			Type = type;
			Class = @class;
			Name = name;
			Array = array;
			Setter = setter;
		}

		public static Variable FromString(string s0) {
			var s = s0.Trim();
			var split = s.Split(' ');
			var array = split[0].Contains("[]");
			var setter = split[1].Contains("{}");

			var type = array ? split[0].Remove(split[0].Length - 2) : split[0];
			var name = setter ? split[1].Remove(split[1].Length - 2) : split[1];
			try
			{
				var enumtype = (VariableType)Enum.Parse(typeof(VariableType), type);
				return new Variable(enumtype, type, name, array, setter);
			} catch {
				return new Variable(VariableType.Class, type, name, array, setter);
			}
		}
		public override string ToString()
		{
			return "Type=" + Type + ", Class=" + Class + ", Name=" + Name + ", Array=" + Array + ", Setter=" + Setter;
		}
	}
}
