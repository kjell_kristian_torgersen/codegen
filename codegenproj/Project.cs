﻿using System;
using System.Collections.Generic;

namespace codegenproj
{
    public class Project
    {
		public DataClass[] DataClasses { get; }
		public Interface[] Interfaces { get; }
		public string Namespace { get; }
        public Dictionary<string, Class> Classes { get; }

		public Project(DataClass[] dataClasses, Interface[] interfaces, string @namespace)
		{
			DataClasses = dataClasses;
			Interfaces = interfaces;
			Namespace = @namespace;
		}
	}
}
